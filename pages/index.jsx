import React from 'react';
import { useEffect, useState } from 'react';
import Fade from 'react-reveal/Fade';
import moment from 'moment';
moment().format();

function Home({ all_sale, sale_latest, not_for_sale , lands, items, bundles, landsSold, itemsSold, bundlesSold, axiesSold}) {
    const [allSaleInfo, setAllSale] = useState(all_sale);
    const [saleLatestInfo, setSaleLatest] = useState(sale_latest);
    const [notForSaleInfo, setNotForSale] = useState(not_for_sale);

    const [landsInfo, setLands] = useState(lands);
    const [itemsInfo, setItems] = useState(items);
    const [bundlesInfo, setBundles] = useState(bundles);

    const [landsSoldInfo, setLandsSold] = useState(landsSold);
    const [itemsSoldInfo, setItemsSold] = useState(itemsSold);
    const [bundlesSoldInfo, setBundlesSold] = useState(bundlesSold);

    const [axiesSoldInfo, setAxiesSold] = useState(axiesSold);

    const [pageCount, setPageCount] = useState(0);

    useEffect(() => {
        setInterval(async() => {
            await calldata().then(data =>{
                setAllSale(data.props.all_sale);
                setSaleLatest(data.props.sale_latest);
                setNotForSale(data.props.not_for_sale);

                setLands(data.props.lands);
                setItems(data.props.items);
                setBundles(data.props.bundles);

                setLandsSold(data.props.landsSold);
                setItemsSold(data.props.itemsSold);
                setBundlesSold(data.props.bundlesSold);

                setAxiesSold(data.props.axiesSold);
            });
        }, 120 * 1000);
    }, []);

    const nextPage = async() =>  {
        setPageCount(pageCount + 1);
        getData(pageCount + 1, 1);
    }

    const prevPage = async() =>  {
        setPageCount(pageCount === 0 ? 0 : pageCount - 1);
        getData(pageCount === 0 ? 0 : pageCount - 1, 0);
    }

    const getData = async(pageCount, control) => {
        await calldata(pageCount, control).then(data =>{
            setAllSale(data.props.all_sale);
            setSaleLatest(data.props.sale_latest);
            setNotForSale(data.props.not_for_sale);

            setLands(data.props.lands);
            setItems(data.props.items);
            setBundles(data.props.bundles);

            setLandsSold(data.props.landsSold);
            setItemsSold(data.props.itemsSold);
            setBundlesSold(data.props.bundlesSold);

            setAxiesSold(data.props.axiesSold);
        });
    }

    return (
        <>
            <div className="container">
                <div className="tab-pane fade show active" id="s" role="tabpanel" aria-labelledby="home-tab">
                    <div className="row align-items-start">
                        <div className="col">
                            <div className="p-3 bg-light">
                                <Fade top>
                                    <p>ALL</p>
                                </Fade>
                                <Fade left>
                                    <h1>{allSaleInfo.axies?.total?.toLocaleString()}</h1>
                                </Fade>
                            </div>
                        </div>
                        <div className="col">
                            <div className="p-3 bg-light">
                                <Fade top>
                                    <p>FOR SALE</p>    
                                </Fade>
                                <Fade bottom>
                                    <h1>{saleLatestInfo.axies?.total?.toLocaleString()}</h1>
                                </Fade>
                            </div>
                        </div>
                        <div className="col">
                            <div className="p-3 bg-light">
                                <Fade top>
                                    <p>NOT FOR SALE</p>
                                </Fade>
                                <Fade right>
                                    <h1>{notForSaleInfo.axies?.total?.toLocaleString()}</h1>
                                </Fade>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <br />
            <br />
            <hr />
            <br />
            <br />

            <div className="container">
                <div className="row g-2">
                    <div className="col-6">
                        <h5>Recenty Listed</h5>
                        <div className="bg-light">
                            <ul className="nav nav-tabs" id="myTab" role="tablist">
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#a" type="button" role="tab" aria-controls="home" aria-selected="true">Axies</button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#l" type="button" role="tab" aria-controls="profile" aria-selected="false">Land</button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#i" type="button" role="tab" aria-controls="contact" aria-selected="false">Items</button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#b" type="button" role="tab" aria-controls="contact" aria-selected="false">Bundles</button>
                                </li>
                            </ul>
                            <div className="tab-content" id="myTabContent" style={{'borderRight': '1px solid #e9ecef', 'borderBottom': '1px solid #e9ecef'}}>
                                <div className="tab-pane fade show active" id="a" role="tabpanel" aria-labelledby="home-tab">
                                    <div className="list-group">
                                        {saleLatestInfo.axies?.results?.map((item) => (
                                            <Fade left>
                                                <a href={`https://marketplace.axieinfinity.com/axie/${item.id}`} target="_blank" className="list-group-item list-group-item-action" aria-current="true">
                                                    <div className="d-flex w-100 justify-content-between">
                                                        <table className="table table-responsive" cellPadding="0" cellSpacing="0" >
                                                            <tr>
                                                                <td width="40%">
                                                                    <img src={`${item.image}`} className="img-thumbnail" width="250" alt={`${item.image}`} />
                                                                </td>
                                                                <td className="text-right">
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">{item.name}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Class: {item.class}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Stage: {item.stage}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Level: {item.level}</span></h5>
                                                                </td>
                                                                <td width="20%">
                                                                    <p className="fs-1"><small>Price $</small> {item.auction.currentPriceUSD}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <small>Breeed Count: <b>{item.breedCount}</b> | Listed: <b>{moment(new Date(item.auction.startingTimestamp * 1000)).fromNow()}</b></small>
                                                </a>
                                            </Fade>
                                            )
                                        )}
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="l" role="tabpanel" aria-labelledby="profile-tab">
                                    <div className="list-group">
                                        {landsInfo.lands?.results?.map((item) => (
                                            <Fade left>
                                                <a href={`https://marketplace.axieinfinity.com/land/${item.row}/${item.col}`} target="_blank" className="list-group-item list-group-item-action" aria-current="true">
                                                    <div className="d-flex w-100 justify-content-between">
                                                        <table className="table table-responsive" cellPadding="0" cellSpacing="0" >
                                                            <tr>
                                                                <td className="text-right">
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Land Type: {item.landType}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Row: {item.row} | Cold: {item.col}</span></h5>
                                                                </td>
                                                                <td width="20%">
                                                                    <p className="fs-1"><small>Price $</small> {item.auction.currentPriceUSD}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <small>Listed: <b>{moment(new Date(item.auction.startingTimestamp * 1000)).fromNow()}</b></small>
                                                </a>
                                            </Fade>
                                            )
                                        )}
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="i" role="tabpanel" aria-labelledby="contact-tab">
                                    <div className="list-group">
                                        {itemsInfo.items?.results?.map((item) => (
                                            <Fade left>
                                                <a href={`https://marketplace.axieinfinity.com/item/${item.itemAlias}/${item.itemId}`} target="_blank" className="list-group-item list-group-item-action" aria-current="true">
                                                    <div className="d-flex w-100 justify-content-between">
                                                        <table className="table table-responsive" cellPadding="0" cellSpacing="0" >
                                                            <tr>
                                                                <td width="40%">
                                                                    <img src={`${item.figureURL}`} className="img-thumbnail" width="250" alt={`${item.figureURL}`} />
                                                                </td>
                                                                <td className="text-right">
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Name: {item.name}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Land Type: {item.landType}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Rarity: {item.rarity}</span></h5>
                                                                </td>
                                                                <td width="20%">
                                                                    <p className="fs-1"><small>Price $</small> {item.auction.currentPriceUSD}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <small><b>{moment(new Date(item.auction.startingTimestamp * 1000)).fromNow()}</b></small>
                                                </a>
                                            </Fade>
                                            )
                                        )}
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="b" role="tabpanel" aria-labelledby="contact-tab">
                                    <div className="list-group">
                                        {bundlesInfo.bundles?.results?.map((item) => (
                                            <Fade left>
                                                <a href={`https://marketplace.axieinfinity.com/bundle/${item.listingIndex}`} target="_blank" className="list-group-item list-group-item-action" aria-current="true">
                                                    <div className="d-flex w-100 justify-content-between">
                                                        <table className="table table-responsive" cellPadding="0" cellSpacing="0" >
                                                            <tr>
                                                                <td className="text-right">
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Name: {item.name}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">bundle: {item.listingIndex}</span></h5>
                                                                </td>
                                                                <td width="20%">
                                                                    <p className="fs-1"><small>Price $</small> {item.auction.currentPriceUSD}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <small><b>{moment(new Date(item.auction.startingTimestamp * 1000)).fromNow()}</b></small>
                                                </a>
                                            </Fade>
                                            )
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-6">
                        <h5>Recenty Sold</h5>
                        <div className="bg-light">
                            <ul className="nav nav-tabs" id="myTab" role="tablist">
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#a2" type="button" role="tab" aria-controls="home" aria-selected="true">Axies</button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#l2" type="button" role="tab" aria-controls="profile" aria-selected="false">Land</button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#i2" type="button" role="tab" aria-controls="contact" aria-selected="false">Items</button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#b2" type="button" role="tab" aria-controls="contact" aria-selected="false">Bundles</button>
                                </li>
                            </ul>
                            <div className="tab-content" id="myTabContent" style={{'borderRight': '1px solid #e9ecef', 'borderBottom': '1px solid #e9ecef'}}>
                                <div className="tab-pane fade show active" id="a2" role="tabpanel" aria-labelledby="home-tab">
                                    <div className="list-group">
                                        { axiesSoldInfo.settledAuctions?.axies?.results?.map((item) => (
                                            <Fade right>
                                                <a href={`https://marketplace.axieinfinity.com/axie/${item.id}`} target="_blank" className="list-group-item list-group-item-action" aria-current="true">
                                                    <div className="d-flex w-100 justify-content-between">
                                                        <table className="table table-responsive" cellPadding="0" cellSpacing="0" >
                                                            <tr>
                                                                <td width="40%">
                                                                    <img src={`${item.image}`} className="img-thumbnail" width="250" alt={`${item.image}`} />
                                                                </td>
                                                                <td className="text-right">
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">{item.name}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Class: {item.class}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Seller: {item.transferHistory.results[0].fromProfile?.name}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Buyer: {item.transferHistory.results[0].toProfile?.name}</span></h5>
                                                                </td>
                                                                <td width="20%">
                                                                    <p className="fs-1"><small>Price $</small> {parseFloat(item.transferHistory.results[0].withPriceUsd).toFixed(2)}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <small>Breeed Count: <b>{item.breedCount}</b> | Listed: <b>{moment(new Date(item.transferHistory.results[0].timestamp * 1000)).fromNow()}</b></small>
                                                </a>
                                            </Fade>
                                            )
                                        )}
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="l2" role="tabpanel" aria-labelledby="profile-tab">
                                    <div className="list-group">
                                        {landsSoldInfo.settledAuctions?.lands?.results?.map((item) => (
                                            <Fade right>
                                                <a href={`https://marketplace.axieinfinity.com/land/${item.row}/${item.col}`} target="_blank" className="list-group-item list-group-item-action" aria-current="true">
                                                    <div className="d-flex w-100 justify-content-between">
                                                        <table className="table table-responsive" cellPadding="0" cellSpacing="0" >
                                                            <tr>
                                                                <td className="text-right">
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Land Type: {item.landType}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Row: {item.row} | Cold: {item.col}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Seller: {item.transferHistory.results[0].fromProfile?.name}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Buyer: {item.transferHistory.results[0].toProfile?.name}</span></h5>
                                                                </td>
                                                                <td width="20%">
                                                                    <p className="fs-1"><small>Price $</small> {parseFloat(item.transferHistory.results[0].withPriceUsd).toFixed(2).toLocaleString()}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <small>Listed: <b>{moment(new Date(item.transferHistory.results[0].timestamp * 1000)).fromNow()}</b></small>
                                                </a>
                                            </Fade>
                                            )
                                        )}
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="i2" role="tabpanel" aria-labelledby="contact-tab">
                                    <div className="list-group">
                                        {itemsSoldInfo.settledAuctions?.items?.results?.map((item) => (
                                            <Fade right>
                                                <a href={`https://marketplace.axieinfinity.com/item/${item.itemAlias}/${item.itemId}`} target="_blank" className="list-group-item list-group-item-action" aria-current="true">
                                                    <div className="d-flex w-100 justify-content-between">
                                                        <table className="table table-responsive" cellPadding="0" cellSpacing="0" >
                                                            <tr>
                                                                <td width="40%">
                                                                    <img src={`${item.figureURL}`} className="img-thumbnail" width="250" alt={`${item.figureURL}`} />
                                                                </td>
                                                                <td className="text-right">
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Name: {item.name}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Rarity: {item.rarity}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Seller: {item.transferHistory.results[0].fromProfile?.name}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Buyer: {item.transferHistory.results[0].toProfile?.name}</span></h5>
                                                                </td>
                                                                <td width="20%">
                                                                    <p className="fs-1"><small>Price $</small> {parseFloat(item.transferHistory.results[0].withPriceUsd).toFixed(2).toLocaleString()}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <small>Listed: <b>{moment(new Date(item.transferHistory.results[0].timestamp * 1000)).fromNow()}</b></small>
                                                </a>
                                            </Fade>
                                            )
                                        )}
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="b2" role="tabpanel" aria-labelledby="contact-tab">
                                    <div className="list-group">
                                        {bundlesSoldInfo.settledAuctions?.bundles?.results?.map((item) => (
                                            <Fade right>
                                                <a href={`https://marketplace.axieinfinity.com/bundle/${item.listingIndex}`} target="_blank" className="list-group-item list-group-item-action" aria-current="true">
                                                    <div className="d-flex w-100 justify-content-between">
                                                        <table className="table table-responsive" cellPadding="0" cellSpacing="0" >
                                                            <tr>
                                                                <td className="text-right">
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Name: {item.name}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">bundle: {item.listingIndex}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Seller: {item.transferHistory.results[0].fromProfile?.name}</span></h5>
                                                                    <h5 className="mb-1"><span className="badge bg-secondary">Buyer: {item.transferHistory.results[0].toProfile?.name}</span></h5>
                                                                </td>
                                                                <td width="20%">
                                                                <p className="fs-1"><small>Price $</small> {parseFloat(item.transferHistory.results[0].withPriceUsd).toFixed(2).toLocaleString()}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <small>Listed: <b>{moment(new Date(item.transferHistory.results[0].timestamp * 1000)).fromNow()}</b></small>
                                                </a>
                                            </Fade>
                                            )
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav aria-label="Page navigation example">
                    <ul className="pagination">
                        <li className="page-item">
                            <a onClick={prevPage} className="page-link" aria-label="Previous"><span aria-hidden="true">Previous &raquo;</span></a>
                        </li>
                        <li className="page-item">
                            <a onClick={nextPage} className="page-link" aria-label="Next"><span aria-hidden="true">Next &raquo;</span></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </>
    );
}

async function calldata(count=0, control=1) {
    let counts = (control === 0 && count !== 0) ? (count - 1) + '1' : count; 
    counts = (control === 1 && count !== 0) ? count + '1' : count; 

    console.log(control === 0 && count !== 0 ? parseInt(counts) + '1' : counts);

    const baseUrl = `https://graphql-gateway.axieinfinity.com/graphql`;
    const params = {
        "operationName": "GetAxieLatest",
        "variables": {
        "from": parseInt(control === 0 && count !== 0 ? parseInt(counts) + '1' : counts),
        "size": 10,
        "sort": "Latest",
        "auctionType": "All",
        "criteria": {}
        },
        "query": "query GetAxieLatest($auctionType: AuctionType, $criteria: AxieSearchCriteria, $from: Int, $sort: SortBy, $size: Int, $owner: String) {\n  axies(auctionType: $auctionType, criteria: $criteria, from: $from, sort: $sort, size: $size, owner: $owner) {\n    total\n    results {\n      ...AxieRowData\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment AxieRowData on Axie {\n  id\n  image\n  class\n  name\n  genes\n  owner\n  class\n  stage\n  title\n  breedCount\n  level\n  parts {\n    ...AxiePart\n    __typename\n  }\n  stats {\n    ...AxieStats\n    __typename\n  }\n  auction {\n    ...AxieAuction\n    __typename\n  }\n  __typename\n}\n\nfragment AxiePart on AxiePart {\n  id\n  name\n  class\n  type\n  specialGenes\n  stage\n  abilities {\n    ...AxieCardAbility\n    __typename\n  }\n  __typename\n}\n\nfragment AxieCardAbility on AxieCardAbility {\n  id\n  name\n  attack\n  defense\n  energy\n  description\n  backgroundUrl\n  effectIconUrl\n  __typename\n}\n\nfragment AxieStats on AxieStats {\n  hp\n  speed\n  skill\n  morale\n  __typename\n}\n\nfragment AxieAuction on Auction {\n  startingPrice\n  endingPrice\n  startingTimestamp\n  endingTimestamp\n  duration\n  timeLeft\n  currentPrice\n  currentPriceUSD\n  suggestedPrice\n  seller\n  listingIndex\n  state\n  __typename\n}\n"
    }

    const params2 = {
        "operationName": "GetAxieLatest",
        "variables": {
        "from": parseInt(control === 0 && count !== 0 ? parseInt(counts) + '1' : counts),
        "size": 10,
        "sort": "Latest",
        "auctionType": "Sale",
        "criteria": {}
        },
        "query": "query GetAxieLatest($auctionType: AuctionType, $criteria: AxieSearchCriteria, $from: Int, $sort: SortBy, $size: Int, $owner: String) {\n  axies(auctionType: $auctionType, criteria: $criteria, from: $from, sort: $sort, size: $size, owner: $owner) {\n    total\n    results {\n      ...AxieRowData\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment AxieRowData on Axie {\n  id\n  image\n  class\n  name\n  genes\n  owner\n  class\n  stage\n  title\n  breedCount\n  level\n  parts {\n    ...AxiePart\n    __typename\n  }\n  stats {\n    ...AxieStats\n    __typename\n  }\n  auction {\n    ...AxieAuction\n    __typename\n  }\n  __typename\n}\n\nfragment AxiePart on AxiePart {\n  id\n  name\n  class\n  type\n  specialGenes\n  stage\n  abilities {\n    ...AxieCardAbility\n    __typename\n  }\n  __typename\n}\n\nfragment AxieCardAbility on AxieCardAbility {\n  id\n  name\n  attack\n  defense\n  energy\n  description\n  backgroundUrl\n  effectIconUrl\n  __typename\n}\n\nfragment AxieStats on AxieStats {\n  hp\n  speed\n  skill\n  morale\n  __typename\n}\n\nfragment AxieAuction on Auction {\n  startingPrice\n  endingPrice\n  startingTimestamp\n  endingTimestamp\n  duration\n  timeLeft\n  currentPrice\n  currentPriceUSD\n  suggestedPrice\n  seller\n  listingIndex\n  state\n  __typename\n}\n"
    }

    const params3 = {
        "operationName": "GetAxieLatest",
        "variables": {
        "from": parseInt(control === 0 && count !== 0 ? parseInt(counts) + '1' : counts),
        "size": 10,
        "sort": "Latest",
        "auctionType": "NotForSale",
        "criteria": {}
        },
        "query": "query GetAxieLatest($auctionType: AuctionType, $criteria: AxieSearchCriteria, $from: Int, $sort: SortBy, $size: Int, $owner: String) {\n  axies(auctionType: $auctionType, criteria: $criteria, from: $from, sort: $sort, size: $size, owner: $owner) {\n    total\n    results {\n      ...AxieRowData\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment AxieRowData on Axie {\n  id\n  image\n  class\n  name\n  genes\n  owner\n  class\n  stage\n  title\n  breedCount\n  level\n  parts {\n    ...AxiePart\n    __typename\n  }\n  stats {\n    ...AxieStats\n    __typename\n  }\n  auction {\n    ...AxieAuction\n    __typename\n  }\n  __typename\n}\n\nfragment AxiePart on AxiePart {\n  id\n  name\n  class\n  type\n  specialGenes\n  stage\n  abilities {\n    ...AxieCardAbility\n    __typename\n  }\n  __typename\n}\n\nfragment AxieCardAbility on AxieCardAbility {\n  id\n  name\n  attack\n  defense\n  energy\n  description\n  backgroundUrl\n  effectIconUrl\n  __typename\n}\n\nfragment AxieStats on AxieStats {\n  hp\n  speed\n  skill\n  morale\n  __typename\n}\n\nfragment AxieAuction on Auction {\n  startingPrice\n  endingPrice\n  startingTimestamp\n  endingTimestamp\n  duration\n  timeLeft\n  currentPrice\n  currentPriceUSD\n  suggestedPrice\n  seller\n  listingIndex\n  state\n  __typename\n}\n"
    }

    const paramsLands = {
        "operationName": "GetLatestLands",
        "variables": {
          "from": parseInt(control === 0 && count !== 0 ? parseInt(counts) + '1' : counts),
          "size": 10,
          "sort": "Latest"
        },
        "query": "query GetLatestLands($from: Int!, $size: Int!, $sort: SortBy!) {\n  lands(from: $from, size: $size, sort: $sort) {\n    results {\n      ...LandBriefV2\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment LandBriefV2 on LandPlot {\n  tokenId\n  owner\n  landType\n  row\n  col\n  auction {\n    currentPrice\n    startingTimestamp\n    currentPriceUSD\n    __typename\n  }\n  ownerProfile {\n    name\n    __typename\n  }\n  __typename\n}\n"
    }

    const paramsItems = {
        "operationName": "GetRecentlyItemList",
        "variables": {
          "from": parseInt(control === 0 && count !== 0 ? parseInt(counts) + '1' : counts),
          "size": 10,
          "sort": "Latest"
        },
        "query": "query GetRecentlyItemList($from: Int, $size: Int, $sort: SortBy) {\n  items(from: $from, size: $size, sort: $sort) {\n    results {\n      ...RecentlyItem\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment RecentlyItem on LandItem {\n  landType\n  rarity\n  name\n  figureURL\n  itemId\n  itemId\n  itemAlias\n  auction {\n    ...AxieAuction\n    __typename\n  }\n  __typename\n}\n\nfragment AxieAuction on Auction {\n  startingPrice\n  endingPrice\n  startingTimestamp\n  endingTimestamp\n  duration\n  timeLeft\n  currentPrice\n  currentPriceUSD\n  suggestedPrice\n  seller\n  listingIndex\n  state\n  __typename\n}\n"
    }

    const paramsBundle = {
        "operationName": "GetBundleLatest",
        "variables": {
          "from": parseInt(control === 0 && count !== 0 ? parseInt(counts) + '1' : counts),
          "size": 10,
          "sort": "Latest"
        },
        "query": "query GetBundleLatest($from: Int!, $size: Int!, $sort: SortBy) {\n  bundles(from: $from, size: $size, sort: $sort) {\n    total\n    results {\n      name\n      listingIndex\n      auction {\n        currentPrice\n        currentPriceUSD\n        currentPrice\n        startingTimestamp\n        __typename\n      }\n      items {\n        ... on LandItem {\n          realTokenId\n          figureURL\n          rarity\n          __typename\n        }\n        ... on LandPlot {\n          realTokenId\n          landType\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n"
    }

    const paramsLandsSold = {
        "operationName": "GetRecentlyLandsSold",
        "variables": {
          "from": parseInt(control === 0 && count !== 0 ? parseInt(counts) + '1' : counts),
          "size": 10
        },
        "query": "query GetRecentlyLandsSold($from: Int, $size: Int) {\n  settledAuctions {\n    lands(from: $from, size: $size) {\n      total\n      results {\n        ...LandSettledBrief\n        transferHistory {\n          ...TransferHistoryInSettledAuction\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment LandSettledBrief on LandPlot {\n  tokenId\n  landType\n  col\n  row\n  __typename\n}\n\nfragment TransferRecordInSettledAuction on TransferRecord {\n  from\n  to\n  txHash\n  timestamp\n  withPrice\n  withPriceUsd\n  fromProfile {\n    name\n    __typename\n  }\n  toProfile {\n    name\n    __typename\n  }\n  __typename\n}\n\nfragment TransferHistoryInSettledAuction on TransferRecords {\n  total\n  results {\n    ...TransferRecordInSettledAuction\n    __typename\n  }\n  __typename\n}\n"
    }

    const paramsItemsSold = {
        "operationName": "GetRecentlyItemsSold",
        "variables": {
          "from": parseInt(control === 0 && count !== 0 ? parseInt(counts) + '1' : counts),
          "size": 10
        },
        "query": "query GetRecentlyItemsSold($from: Int, $size: Int) {\n  settledAuctions {\n    items(from: $from, size: $size) {\n      total\n      results {\n        ...ItemSettledBrief\n        transferHistory {\n          ...TransferHistoryInSettledAuction\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment ItemSettledBrief on LandItem {\n  itemId\n  name\n  itemAlias\n  itemId\n  figureURL\n  rarity\n  __typename\n}\n\nfragment TransferHistoryInSettledAuction on TransferRecords {\n  total\n  results {\n    ...TransferRecordInSettledAuction\n    __typename\n  }\n  __typename\n}\n\nfragment TransferRecordInSettledAuction on TransferRecord {\n  from\n  to\n  txHash\n  timestamp\n  withPrice\n  withPriceUsd\n  fromProfile {\n    name\n    __typename\n  }\n  toProfile {\n    name\n    __typename\n  }\n  __typename\n}\n"
    }

    const paramsBundleSold = {
        "operationName": "GetRecentlyBundlesSold",
        "variables": {
          "from": parseInt(control === 0 && count !== 0 ? parseInt(counts) + '1' : counts),
          "size": 10
        },
        "query": "query GetRecentlyBundlesSold($from: Int, $size: Int) {\n  settledAuctions {\n    bundles(from: $from, size: $size) {\n      total\n      results {\n        listingIndex\n        ...BundleDetail\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment BundleDetail on Bundle {\n  name\n  owner\n  auction {\n    ...AxieAuction\n    __typename\n  }\n  items {\n    ...LandBundleBrief\n    ...ItemBundleBrief\n    __typename\n  }\n  transferHistory {\n    ...TransferHistoryInSettledAuction\n    __typename\n  }\n  __typename\n}\n\nfragment LandBundleBrief on LandPlot {\n  landType\n  row\n  col\n  __typename\n}\n\nfragment ItemBundleBrief on LandItem {\n  landType\n  name\n  itemAlias\n  rarity\n  figureURL\n  tokenId\n  itemId\n  __typename\n}\n\nfragment AxieAuction on Auction {\n  startingPrice\n  endingPrice\n  startingTimestamp\n  endingTimestamp\n  duration\n  timeLeft\n  currentPrice\n  currentPriceUSD\n  suggestedPrice\n  seller\n  listingIndex\n  state\n  __typename\n}\n\nfragment TransferHistoryInSettledAuction on TransferRecords {\n  total\n  results {\n    ...TransferRecordInSettledAuction\n    __typename\n  }\n  __typename\n}\n\nfragment TransferRecordInSettledAuction on TransferRecord {\n  from\n  to\n  txHash\n  timestamp\n  withPrice\n  withPriceUsd\n  fromProfile {\n    name\n    __typename\n  }\n  toProfile {\n    name\n    __typename\n  }\n  __typename\n}\n"
    }

    const paramsAxiesSold = {
        "operationName": "GetRecentlyAxiesSold",
        "variables": {
          "from": parseInt(control === 0 && count !== 0 ? parseInt(counts) + '1' : counts),
          "size": 10
        },
        "query": "query GetRecentlyAxiesSold($from: Int, $size: Int) {\n  settledAuctions {\n    axies(from: $from, size: $size) {\n      total\n      results {\n        ...AxieSettledBrief\n        transferHistory {\n          ...TransferHistoryInSettledAuction\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment AxieSettledBrief on Axie {\n  id\n  name\n  image\n  class\n  breedCount\n  __typename\n}\n\nfragment TransferHistoryInSettledAuction on TransferRecords {\n  total\n  results {\n    ...TransferRecordInSettledAuction\n    __typename\n  }\n  __typename\n}\n\nfragment TransferRecordInSettledAuction on TransferRecord {\n  from\n  to\n  txHash\n  timestamp\n  withPrice\n  withPriceUsd\n  fromProfile {\n    name\n    __typename\n  }\n  toProfile {\n    name\n    __typename\n  }\n  __typename\n}\n"
    }

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(params)
    };

    const requestOptions2 = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(params2)
    };

    const requestOptions3 = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(params3)
    };

    const requestOptions4 = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(paramsLands)
    };

    const requestOptions5 = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(paramsItems)
    };

    const requestOptions6 = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(paramsBundle)
    };

    const requestOptions7 = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(paramsLandsSold)
    };

    const requestOptions8 = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(paramsItemsSold)
    };

    const requestOptions9 = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(paramsBundleSold)
    };

    const requestOptions10 = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(paramsAxiesSold)
    };

    const all = await fetch(baseUrl, requestOptions);
    const all_sale = await all.json()

    const sale = await fetch(baseUrl, requestOptions2);
    const sale_latest = await sale.json()

    const not_sale = await fetch(baseUrl, requestOptions3);
    const not_for_sale = await not_sale.json()

    const land_get = await fetch(baseUrl, requestOptions4);
    const lands = await land_get.json()

    const item_get = await fetch(baseUrl, requestOptions5);
    const items = await item_get.json()

    const get_bundle = await fetch(baseUrl, requestOptions6);
    const bundles = await get_bundle.json()

    const land_sold_get = await fetch(baseUrl, requestOptions7);
    const landsSold = await land_sold_get.json()

    const item_sold_get = await fetch(baseUrl, requestOptions8);
    const itemsSold = await item_sold_get.json()

    const get_sold_bundle = await fetch(baseUrl, requestOptions9);
    const bundlesSold = await get_sold_bundle.json()

    const get_sold_axies = await fetch(baseUrl, requestOptions10);
    const axiesSold = await get_sold_axies.json()

    return { 
        // props: {
        //     all_sale: [],
        //     sale_latest: [],
        //     not_for_sale: [],
        //     lands: [],
        //     items: [],
        //     bundles: [],
        //     landsSold: [],
        //     itemsSold: [],
        //     bundlesSold: [],
        //     axiesSold: [],
        // }
        props: {
            all_sale: all_sale.data,
            sale_latest: sale_latest.data,
            not_for_sale: not_for_sale.data,
            lands: lands.data,
            items: items.data,
            bundles: bundles.data,
            landsSold: landsSold.data,
            itemsSold: itemsSold.data,
            bundlesSold: bundlesSold.data,
            axiesSold: axiesSold.data,
        }
    }
}

export async function getServerSideProps(clx) {
    return calldata();
}

export default Home;
