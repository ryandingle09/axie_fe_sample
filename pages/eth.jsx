import React from 'react';
import Fade from 'react-reveal/Fade';

function Ethereum({ all_sale, sale_latest, not_for_sale }) {
    console.log(all_sale.axies.total);
    console.log(sale_latest.axies.total);
    console.log(not_for_sale.axies.total);
    return (
        <>
            <div className="container">
                <div className="tab-pane fade show active" id="s" role="tabpanel" aria-labelledby="home-tab">
                    <div className="row align-items-start">
                        <div className="col">
                            <div className="p-3 bg-light">
                                <Fade top>
                                    <p>ALL</p>
                                </Fade>
                                <Fade left>
                                    <h1>{all_sale.axies.total.toLocaleString()}</h1>
                                </Fade>
                            </div>
                        </div>
                        <div className="col">
                            <div className="p-3 bg-light">
                                <Fade top>
                                    <p>FOR SALE</p>    
                                </Fade>
                                <Fade bottom>
                                    <h1>{sale_latest.axies.total.toLocaleString()}</h1>
                                </Fade>
                            </div>
                        </div>
                        <div className="col">
                            <div className="p-3 bg-light">
                                <Fade top>
                                    <p>NOT FOR SALE</p>
                                </Fade>
                                <Fade right>
                                    <h1>{not_for_sale.axies.total.toLocaleString()}</h1>
                                </Fade>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <br />
            <br />
            <hr />
            <br />
            <br />

            <div className="container">
                <div className="row g-2">
                    <div className="col-6">
                        <h5>Recenty Listed</h5>
                        <div className="bg-light">
                            <ul className="nav nav-tabs" id="myTab" role="tablist">
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#a" type="button" role="tab" aria-controls="home" aria-selected="true">Axies</button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#l" type="button" role="tab" aria-controls="profile" aria-selected="false">Land</button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#i" type="button" role="tab" aria-controls="contact" aria-selected="false">Items</button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#b" type="button" role="tab" aria-controls="contact" aria-selected="false">Bundles</button>
                                </li>
                            </ul>
                            <div className="tab-content" id="myTabContent" style={{'borderRight': '1px solid #e9ecef', 'borderBottom': '1px solid #e9ecef'}}>
                                <div className="tab-pane fade show active" id="a" role="tabpanel" aria-labelledby="home-tab">
                                    a
                                </div>
                                <div className="tab-pane fade" id="l" role="tabpanel" aria-labelledby="profile-tab">
                                    l
                                </div>
                                <div className="tab-pane fade" id="i" role="tabpanel" aria-labelledby="contact-tab">
                                    i
                                </div>
                                <div className="tab-pane fade" id="b" role="tabpanel" aria-labelledby="contact-tab">
                                    b
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-6">
                        <h5>Recenty Sold</h5>
                        <div className="bg-light">
                            <ul className="nav nav-tabs" id="myTab" role="tablist">
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#a2" type="button" role="tab" aria-controls="home" aria-selected="true">Axies</button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#l2" type="button" role="tab" aria-controls="profile" aria-selected="false">Land</button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#i2" type="button" role="tab" aria-controls="contact" aria-selected="false">Items</button>
                                </li>
                                <li className="nav-item" role="presentation">
                                    <button className="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#b2" type="button" role="tab" aria-controls="contact" aria-selected="false">Bundles</button>
                                </li>
                            </ul>
                            <div className="tab-content" id="myTabContent" style={{'borderRight': '1px solid #e9ecef', 'borderBottom': '1px solid #e9ecef'}}>
                                <div className="tab-pane fade show active" id="a2" role="tabpanel" aria-labelledby="home-tab">
                                    a
                                </div>
                                <div className="tab-pane fade" id="l2" role="tabpanel" aria-labelledby="profile-tab">
                                    l
                                </div>
                                <div className="tab-pane fade" id="i2" role="tabpanel" aria-labelledby="contact-tab">
                                    i
                                </div>
                                <div className="tab-pane fade" id="b2" role="tabpanel" aria-labelledby="contact-tab">
                                    b
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export async function getStaticProps({ req, res }) {
    // res.setHeader(
    //     'Cache-Control',
    //     'public, s-maxage=10, stale-while-revalidate=59'
    // )
    const baseUrl = `https://graphql-gateway.axieinfinity.com/graphql`;
    const params = {
        "operationName": "GetAxieLatest",
        "variables": {
        "from": 0,
        "size": 10,
        "sort": "Latest",
        "auctionType": "All",
        "criteria": {}
        },
        "query": "query GetAxieLatest($auctionType: AuctionType, $criteria: AxieSearchCriteria, $from: Int, $sort: SortBy, $size: Int, $owner: String) {\n  axies(auctionType: $auctionType, criteria: $criteria, from: $from, sort: $sort, size: $size, owner: $owner) {\n    total\n    results {\n      ...AxieRowData\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment AxieRowData on Axie {\n  id\n  image\n  class\n  name\n  genes\n  owner\n  class\n  stage\n  title\n  breedCount\n  level\n  parts {\n    ...AxiePart\n    __typename\n  }\n  stats {\n    ...AxieStats\n    __typename\n  }\n  auction {\n    ...AxieAuction\n    __typename\n  }\n  __typename\n}\n\nfragment AxiePart on AxiePart {\n  id\n  name\n  class\n  type\n  specialGenes\n  stage\n  abilities {\n    ...AxieCardAbility\n    __typename\n  }\n  __typename\n}\n\nfragment AxieCardAbility on AxieCardAbility {\n  id\n  name\n  attack\n  defense\n  energy\n  description\n  backgroundUrl\n  effectIconUrl\n  __typename\n}\n\nfragment AxieStats on AxieStats {\n  hp\n  speed\n  skill\n  morale\n  __typename\n}\n\nfragment AxieAuction on Auction {\n  startingPrice\n  endingPrice\n  startingTimestamp\n  endingTimestamp\n  duration\n  timeLeft\n  currentPrice\n  currentPriceUSD\n  suggestedPrice\n  seller\n  listingIndex\n  state\n  __typename\n}\n"
    }

    const params2 = {
        "operationName": "GetAxieLatest",
        "variables": {
        "from": 0,
        "size": 10,
        "sort": "Latest",
        "auctionType": "Sale",
        "criteria": {}
        },
        "query": "query GetAxieLatest($auctionType: AuctionType, $criteria: AxieSearchCriteria, $from: Int, $sort: SortBy, $size: Int, $owner: String) {\n  axies(auctionType: $auctionType, criteria: $criteria, from: $from, sort: $sort, size: $size, owner: $owner) {\n    total\n    results {\n      ...AxieRowData\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment AxieRowData on Axie {\n  id\n  image\n  class\n  name\n  genes\n  owner\n  class\n  stage\n  title\n  breedCount\n  level\n  parts {\n    ...AxiePart\n    __typename\n  }\n  stats {\n    ...AxieStats\n    __typename\n  }\n  auction {\n    ...AxieAuction\n    __typename\n  }\n  __typename\n}\n\nfragment AxiePart on AxiePart {\n  id\n  name\n  class\n  type\n  specialGenes\n  stage\n  abilities {\n    ...AxieCardAbility\n    __typename\n  }\n  __typename\n}\n\nfragment AxieCardAbility on AxieCardAbility {\n  id\n  name\n  attack\n  defense\n  energy\n  description\n  backgroundUrl\n  effectIconUrl\n  __typename\n}\n\nfragment AxieStats on AxieStats {\n  hp\n  speed\n  skill\n  morale\n  __typename\n}\n\nfragment AxieAuction on Auction {\n  startingPrice\n  endingPrice\n  startingTimestamp\n  endingTimestamp\n  duration\n  timeLeft\n  currentPrice\n  currentPriceUSD\n  suggestedPrice\n  seller\n  listingIndex\n  state\n  __typename\n}\n"
    }

    const params3 = {
        "operationName": "GetAxieLatest",
        "variables": {
        "from": 0,
        "size": 10,
        "sort": "Latest",
        "auctionType": "NotForSale",
        "criteria": {}
        },
        "query": "query GetAxieLatest($auctionType: AuctionType, $criteria: AxieSearchCriteria, $from: Int, $sort: SortBy, $size: Int, $owner: String) {\n  axies(auctionType: $auctionType, criteria: $criteria, from: $from, sort: $sort, size: $size, owner: $owner) {\n    total\n    results {\n      ...AxieRowData\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment AxieRowData on Axie {\n  id\n  image\n  class\n  name\n  genes\n  owner\n  class\n  stage\n  title\n  breedCount\n  level\n  parts {\n    ...AxiePart\n    __typename\n  }\n  stats {\n    ...AxieStats\n    __typename\n  }\n  auction {\n    ...AxieAuction\n    __typename\n  }\n  __typename\n}\n\nfragment AxiePart on AxiePart {\n  id\n  name\n  class\n  type\n  specialGenes\n  stage\n  abilities {\n    ...AxieCardAbility\n    __typename\n  }\n  __typename\n}\n\nfragment AxieCardAbility on AxieCardAbility {\n  id\n  name\n  attack\n  defense\n  energy\n  description\n  backgroundUrl\n  effectIconUrl\n  __typename\n}\n\nfragment AxieStats on AxieStats {\n  hp\n  speed\n  skill\n  morale\n  __typename\n}\n\nfragment AxieAuction on Auction {\n  startingPrice\n  endingPrice\n  startingTimestamp\n  endingTimestamp\n  duration\n  timeLeft\n  currentPrice\n  currentPriceUSD\n  suggestedPrice\n  seller\n  listingIndex\n  state\n  __typename\n}\n"
    }

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(params)
    };

    const requestOptions2 = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(params2)
    };

    const requestOptions3 = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(params3)
    };

    const all = await fetch(baseUrl, requestOptions);
    const all_sale = await all.json()

    const sale = await fetch(baseUrl, requestOptions2);
    const sale_latest = await sale.json()

    const not_sale = await fetch(baseUrl, requestOptions3);
    const not_for_sale = await not_sale.json()

    return { 
        props: {
            all_sale: all_sale.data,
            sale_latest: sale_latest.data,
            not_for_sale: not_for_sale.data,
        }
    }
}

export default Ethereum;